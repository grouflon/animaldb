#include "stdafx.h"
#include "AnimalDatabase.h"

#include <dirent.h>
#include <core/JsonTools.h>
#include <core/File.h>

AnimalDatabase::AnimalDatabase()
{
	
}

AnimalDatabase::~AnimalDatabase()
{
	
}

void AnimalDatabase::Load(const char* _path)
{
	LoadNode(nullptr, ANIMALDATABASE_ROOT_TAG, _path);
}

void AnimalDatabase::Release()
{
	for (auto pair : m_nodes)
	{
		delete pair.second;
	}
	m_nodes.clear();
}

void AnimalDatabase::LoadNode(Node* _parent, const char* _name, const char* _path)
{
	std::string dirPath = _path;

	if (dirPath.back() != '/' && dirPath.back() != '\\')
	{
		dirPath += '/';
	}

	DIR* dir = opendir(dirPath.c_str());

	if (!dir)
	{
		LOG_ERROR("Failed to create Node \"%s\": Unable to open \"%s\"", _name, dirPath.c_str());
		return;
	}

	Node* node = new Node();
	node->m_name = _name;
	node->m_parent = _parent;
	m_nodes.insert(std::make_pair(_name, node));

	std::string subPath;
	while (dirent* ent = readdir(dir))
	{
		if (ent->d_name[0] == '.')
		{
			continue;
		}

		subPath = dirPath + ent->d_name;
		if (ent->d_type == DT_DIR)
		{
			LoadNode(node, ent->d_name, subPath.c_str());
		}
		else
		{
			File data(subPath.c_str());
			data.load();
			if (data.getError() != Resource::ERROR_NONE)
			{
				LOG_ERROR("%s", data.getErrorDescription());
				data.release();
				continue;
			}

			json_value_s* jsonData = json_parse(data.getContent(), data.getSize());
			data.release();
			if (!jsonData)
			{
				LOG_ERROR("Error reading \"%s\": Malformed JSON file", subPath.c_str());
				continue;
			}

			json_value_s* classNameValue = FindJsonValue(jsonData, "className");
			if (!classNameValue || classNameValue->type != json_type_string)
			{
				LOG_ERROR("Error reading \"%s\": Missing or non-string \"className\" value", subPath.c_str());
				free(jsonData);
				continue;
			}

			node->m_data.insert(std::make_pair(JsonValueToString(classNameValue), jsonData));
		}
	}

	closedir(dir);
}
