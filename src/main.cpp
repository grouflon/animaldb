#include "stdafx.h"


#include <core/Reflection.h>

#include "AnimalDatabase.h"

#include <vld.h>

struct CameraData
{
	std::string name;

	REFLECT_BEGIN(CameraData)
	REFLECT_AUTO(name)
	REFLECT_END()
};

struct BehaviorData
{
	std::string name;
	bool overwritten;

	REFLECT_BEGIN(BehaviorData)
	REFLECT_AUTO(name)
	REFLECT_AUTO(overwritten)
	REFLECT_END()
};

int main(int argc, char** argv)
{
	AnimalDatabase db;
	db.Load("data/animalDB");

	CameraData camera;
	BehaviorData behavior;

	if (!db.FindData(&behavior, "eagle"))
	{
		LOG_ERROR("Behavior problem ?!");
	}
	if (!db.FindData(&camera, "birds"))
	{
		LOG_ERROR("Camera problem ?!");
	}

	db.Release();
	return EXIT_SUCCESS;
}