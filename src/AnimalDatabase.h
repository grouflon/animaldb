#pragma once

#define ANIMALDATABASE_ROOT_TAG "default"

#include <json.h>
#include <core/JsonSerializer.h>

class AnimalDatabase
{
public:
	AnimalDatabase();
	~AnimalDatabase();

	void Load(const char* _path);
	void Release();

	template <typename T>
	bool FindData(T* _data, const char* _tag = ANIMALDATABASE_ROOT_TAG) const;

private:

	struct Node
	{
		~Node()
		{
			for (auto pair : m_data)
			{
				free(pair.second);
			}
		}

		Node* m_parent;
		HashMap<json_value_s*> m_data;
		std::string m_name; // Not sure if useful
	};

	template <typename T>
	bool SerializeNode(T* _data, const Node* _node) const;
	
	void LoadNode(Node* _parent, const char* _name, const char* _path);

	HashMap<Node*> m_nodes;
};


template <typename T>
bool AnimalDatabase::FindData(T* _data, const char* _tag) const
{
	auto it = m_nodes.find(_tag);
	if (it != m_nodes.end())
	{
		return SerializeNode(_data, it->second);
	}
	return false;
}


template <typename T>
bool AnimalDatabase::SerializeNode(T* _data, const Node* _node) const
{
	bool result = false;
	if (_node->m_parent)
	{
		result = SerializeNode(_data, _node->m_parent);
	}

	auto it = _node->m_data.find(T::getClassDesc()->getName());
	if (it != _node->m_data.end())
	{
		json_value_s val;
		json_object_s obj;
		json_object_element_s elem;
		json_string_s name;
		val.type = json_type_object;
		val.payload = &obj;
		obj.start = &elem;
		obj.length = 1;
		elem.name = &name;
		elem.value = it->second;
		elem.next = nullptr;
		name.string = "data";
		name.string_size = 4;

		JsonSerializer serializer;
		serializer.beginRead(&val);
		result = serializer.serialize("data", _data) || result;
		serializer.end();
	}
	return result;
}