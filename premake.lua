solution "animaldb"
	platforms { "Win32" }
	configurations { "debug", "release" }
	
	location("project/" .. _ACTION)

	startproject "animaldb"
	
	basedir(_WORKING_DIR .. "extern/core/")
	include "extern/core/core_premake"
	basedir(_WORKING_DIR)
	
	project "animaldb"
		kind "ConsoleApp"
		language "C++"
		architecture "x86"
		flags { "Symbols", "MultiProcessorCompile" }
		rtti "On"
		editandcontinue "On"
		
		targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
		objdir "obj/%{cfg.platform}/%{cfg.buildcfg}/"
		
		debugdir "."
		
		pchheader "stdafx.h"
		pchsource "src/stdafx.cpp"
		
		files {
			"src/**.cpp",
			"src/**.inl",
			"src/**.c",
			"src/**.h",
		}
		
		removefiles {
			"src/main-test.cpp",
		}
		
		includedirs {
			"src/",
			"extern/core/include",
			"extern/core/extern/gl3w/",
			"extern/core/extern/glm/",
			"extern/core/extern/json/",
			"extern/core/extern/imgui/",
			"extern/core/extern/SDL2-2.0.4/include",
			"extern/core/extern/portaudio/include",
		}
		
		libdirs {
			"extern/core/libs/%{cfg.platform}/%{cfg.buildcfg}/",
			"extern/core/extern/SDL2-2.0.4/lib/%{cfg.architecture}/",
			"extern/core/extern/portaudio/lib/%{cfg.platform}/%{cfg.buildcfg}/",
		}
			
		links {
			"core",
			"opengl32",
			"SDL2",
			"legacy_stdio_definitions",
			"portaudio_%{cfg.architecture}"
		}
		
		postbuildcommands {
			"{COPY} ../../extern/core/extern/SDL2-2.0.4/lib/%{cfg.architecture}/SDL2.dll ../../bin/%{cfg.platform}/%{cfg.buildcfg}",
			"{COPY} ../../extern/core/extern/portaudio/lib/%{cfg.platform}/%{cfg.buildcfg}/portaudio_%{cfg.architecture}.dll ../../bin/%{cfg.platform}/%{cfg.buildcfg}"
		}
		
		filter "configurations:debug"
			optimize "Off"
			defines "_DEBUG"
			
		filter "configurations:release"
			optimize "Full"
			
		filter "action:vs2012"
			toolset "v120_CTP_Nov2012"
